package ru.arubtsova.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.repository.ICommandRepository;
import ru.arubtsova.tm.api.service.EndpointLocator;
import ru.arubtsova.tm.api.service.ICommandService;
import ru.arubtsova.tm.api.service.ILogService;
import ru.arubtsova.tm.api.service.IPropertyService;
import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.component.FileScanner;
import ru.arubtsova.tm.endpoint.*;
import ru.arubtsova.tm.exception.system.UnknownCommandException;
import ru.arubtsova.tm.repository.CommandRepository;
import ru.arubtsova.tm.service.CommandService;
import ru.arubtsova.tm.service.LogService;
import ru.arubtsova.tm.service.PropertyService;
import ru.arubtsova.tm.util.SystemUtil;
import ru.arubtsova.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Getter
@Setter
public class Bootstrap implements EndpointLocator {

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final ProjectTaskEndpointService projectTaskEndpointService = new ProjectTaskEndpointService();

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint = projectTaskEndpointService.getProjectTaskEndpointPort();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this, propertyService);

    private Session session;

    private void initCommands(@NotNull final List<AbstractCommand> commandList) {
        for (@NotNull final AbstractCommand command : commandList) {
            registry(command);
        }
    }

    {
        initCommands(commandService.getCommandList());
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void run(@Nullable final String... args) {
        logService.debug("Application started");
        logService.info("*** WELCOME TO TASK-MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        initPID();
        fileScanner.init();
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.println("Enter Command:");
            try {
                @NotNull final String command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                System.out.println("Ok");
            } catch (@NotNull final Exception e) {
                logService.error(e);
                System.out.println("Fail");
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    public void parseArg(@Nullable final String arg) {
        if (!Optional.ofNullable(arg).isPresent() || arg.isEmpty()) return;
        @Nullable final AbstractCommand command = arguments.get(arg);
        if (!Optional.ofNullable(command).isPresent()) return;
        command.execute();
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(@Nullable final String cmd) {
        if (!Optional.ofNullable(cmd).isPresent() || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownCommandException(cmd);
        @Nullable final Role[] roles = command.roles();
        //authService.checkRole(roles);
        command.execute();
    }

}
