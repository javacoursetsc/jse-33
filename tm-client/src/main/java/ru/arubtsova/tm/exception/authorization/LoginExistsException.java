package ru.arubtsova.tm.exception.authorization;

import ru.arubtsova.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException() {
        super("Error! Login Already Exists, Please Try Another One...");
    }

}
