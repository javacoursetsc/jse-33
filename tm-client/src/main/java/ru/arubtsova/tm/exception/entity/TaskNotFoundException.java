package ru.arubtsova.tm.exception.entity;

import ru.arubtsova.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
