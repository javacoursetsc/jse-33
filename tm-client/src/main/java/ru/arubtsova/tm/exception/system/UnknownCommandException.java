package ru.arubtsova.tm.exception.system;

import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(@Nullable final String command) {
        super("Error! Unknown command ``" + command + "``...");
    }

}
