package ru.arubtsova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractProjectCommand;
import ru.arubtsova.tm.endpoint.Project;
import ru.arubtsova.tm.endpoint.Session;
import ru.arubtsova.tm.exception.entity.ProjectNotFoundException;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectStartByNameCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-start-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "change project status to In progress by project name.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Project:");
        System.out.println("Enter Project Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        final Project project = endpointLocator.getProjectEndpoint().startByNameProject(session, name);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

}
