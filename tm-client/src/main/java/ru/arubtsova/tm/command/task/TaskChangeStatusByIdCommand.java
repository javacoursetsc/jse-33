package ru.arubtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.endpoint.Session;
import ru.arubtsova.tm.endpoint.Status;
import ru.arubtsova.tm.endpoint.Task;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-change-status-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "change task status by task id.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Task:");
        System.out.println("Enter Task Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("Enter Task Status:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusId);
        final Task task = endpointLocator.getTaskEndpoint().findTaskByIdWithUserId(session, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        final Task taskStatusUpdate = endpointLocator.getTaskEndpoint().changeStatusById(session, id, status);
        Optional.ofNullable(taskStatusUpdate).orElseThrow(TaskNotFoundException::new);
        System.out.println("Task status was successfully updated");
    }

}
