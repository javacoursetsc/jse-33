package ru.arubtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.endpoint.Session;
import ru.arubtsova.tm.endpoint.Task;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskBindToProjectCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-bind-to-project";
    }

    @NotNull
    @Override
    public String description() {
        return "bind task to project.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Enter Task Id:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        System.out.println("Enter Project Id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final Task task = endpointLocator.getProjectTaskEndpoint().bindTaskToProject(session, taskId, projectId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("Task was successfully bound to Project");
    }

}
