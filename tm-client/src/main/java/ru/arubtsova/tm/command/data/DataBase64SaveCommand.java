package ru.arubtsova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractDataCommand;
import ru.arubtsova.tm.endpoint.Session;

public class DataBase64SaveCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-save";
    }

    @NotNull
    @Override
    public String description() {
        return "save base64 data to file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("Data base64 save:");
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getAdminEndpoint().saveBase64(session);
        System.out.println("Successful");
    }

}
