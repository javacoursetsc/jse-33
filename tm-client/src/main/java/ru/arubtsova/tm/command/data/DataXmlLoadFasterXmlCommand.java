package ru.arubtsova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractDataCommand;
import ru.arubtsova.tm.endpoint.Session;

public class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-load-fasterxml";
    }

    @NotNull
    @Override
    public String description() {
        return "load xml data from file by FasterXml library.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("Data xml load:");
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getAdminEndpoint().loadXMLFasterXML(session);
        System.out.println("Successful");
    }

}
