package ru.arubtsova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractDataCommand;
import ru.arubtsova.tm.endpoint.Session;

public class BackupLoadCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "backup-load";
    }

    @NotNull
    @Override
    public String description() {
        return "load system backup from xml file by FasterXml library.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("Data backup load:");
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getAdminEndpoint().loadBackup(session);
        System.out.println("Successful");
    }

}
