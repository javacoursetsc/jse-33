package ru.arubtsova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.command.AbstractCommand;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "show terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("Help:");
        @NotNull final Collection<AbstractCommand> commands = endpointLocator.getCommandService().getCommandList();
        for (@NotNull final AbstractCommand command : commands)
            System.out.println(command.name() + " - " + command.description());
    }

}