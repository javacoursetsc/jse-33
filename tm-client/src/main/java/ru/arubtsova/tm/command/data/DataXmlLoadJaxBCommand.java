package ru.arubtsova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractDataCommand;
import ru.arubtsova.tm.endpoint.Session;

public class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-load-jaxb";
    }

    @NotNull
    @Override
    public String description() {
        return "load xml data from file by JaxB library.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("Data xml load by JaxB:");
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getAdminEndpoint().loadXMLJaxb(session);
        System.out.println("Successful");
    }

}
