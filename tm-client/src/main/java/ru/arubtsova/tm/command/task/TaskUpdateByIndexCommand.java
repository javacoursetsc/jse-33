package ru.arubtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.endpoint.Session;
import ru.arubtsova.tm.endpoint.Task;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "update a task by index.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Task:");
        System.out.println("Enter Task Index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = endpointLocator.getTaskEndpoint().findByIndex(session, index);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("Enter New Task Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter New Task Description:");
        @NotNull final String description = TerminalUtil.nextLine();
        final Task taskUpdate = endpointLocator.getTaskEndpoint().updateByIndex(session, index, name, description);
        Optional.ofNullable(taskUpdate).orElseThrow(TaskNotFoundException::new);
        System.out.println("Task was successfully updated");
    }

}
