package ru.arubtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.endpoint.Session;
import ru.arubtsova.tm.endpoint.Task;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskShowByNameCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-view-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "find a task by name.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Task Overview:");
        System.out.println("Enter Task Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        final Task task = endpointLocator.getTaskEndpoint().findByName(session, name);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

}
