package ru.arubtsova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractProjectCommand;
import ru.arubtsova.tm.endpoint.Project;
import ru.arubtsova.tm.endpoint.Session;

import java.util.List;

public class ProjectShowAllCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "show all projects, sort them.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Project List:");
        @Nullable List<Project> projects = endpointLocator.getProjectEndpoint().findAllProjectWithUserId(session);
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + project.getName() + " - " + project.getDescription());
            index++;
        }
    }

}
