package ru.arubtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.endpoint.*;

public interface EndpointLocator {

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    ProjectTaskEndpoint getProjectTaskEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    Session getSession();

    void setSession(Session session);

    @NotNull
    ICommandService getCommandService();

}
