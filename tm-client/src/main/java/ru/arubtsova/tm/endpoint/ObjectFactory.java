package ru.arubtsova.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the ru.arubtsova.tm.endpoint package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Register_QNAME = new QName("http://endpoint.tm.arubtsova.ru/", "register");
    private final static QName _RegisterResponse_QNAME = new QName("http://endpoint.tm.arubtsova.ru/", "registerResponse");
    private final static QName _SetPassword_QNAME = new QName("http://endpoint.tm.arubtsova.ru/", "setPassword");
    private final static QName _SetPasswordResponse_QNAME = new QName("http://endpoint.tm.arubtsova.ru/", "setPasswordResponse");
    private final static QName _UpdateUser_QNAME = new QName("http://endpoint.tm.arubtsova.ru/", "updateUser");
    private final static QName _UpdateUserResponse_QNAME = new QName("http://endpoint.tm.arubtsova.ru/", "updateUserResponse");
    private final static QName _ViewProfile_QNAME = new QName("http://endpoint.tm.arubtsova.ru/", "viewProfile");
    private final static QName _ViewProfileResponse_QNAME = new QName("http://endpoint.tm.arubtsova.ru/", "viewProfileResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.arubtsova.tm.endpoint
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Register }
     */
    public Register createRegister() {
        return new Register();
    }

    /**
     * Create an instance of {@link RegisterResponse }
     */
    public RegisterResponse createRegisterResponse() {
        return new RegisterResponse();
    }

    /**
     * Create an instance of {@link SetPassword }
     */
    public SetPassword createSetPassword() {
        return new SetPassword();
    }

    /**
     * Create an instance of {@link SetPasswordResponse }
     */
    public SetPasswordResponse createSetPasswordResponse() {
        return new SetPasswordResponse();
    }

    /**
     * Create an instance of {@link UpdateUser }
     */
    public UpdateUser createUpdateUser() {
        return new UpdateUser();
    }

    /**
     * Create an instance of {@link UpdateUserResponse }
     */
    public UpdateUserResponse createUpdateUserResponse() {
        return new UpdateUserResponse();
    }

    /**
     * Create an instance of {@link ViewProfile }
     */
    public ViewProfile createViewProfile() {
        return new ViewProfile();
    }

    /**
     * Create an instance of {@link ViewProfileResponse }
     */
    public ViewProfileResponse createViewProfileResponse() {
        return new ViewProfileResponse();
    }

    /**
     * Create an instance of {@link Session }
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link User }
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Register }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.arubtsova.ru/", name = "register")
    public JAXBElement<Register> createRegister(Register value) {
        return new JAXBElement<Register>(_Register_QNAME, Register.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.arubtsova.ru/", name = "registerResponse")
    public JAXBElement<RegisterResponse> createRegisterResponse(RegisterResponse value) {
        return new JAXBElement<RegisterResponse>(_RegisterResponse_QNAME, RegisterResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetPassword }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.arubtsova.ru/", name = "setPassword")
    public JAXBElement<SetPassword> createSetPassword(SetPassword value) {
        return new JAXBElement<SetPassword>(_SetPassword_QNAME, SetPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetPasswordResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.arubtsova.ru/", name = "setPasswordResponse")
    public JAXBElement<SetPasswordResponse> createSetPasswordResponse(SetPasswordResponse value) {
        return new JAXBElement<SetPasswordResponse>(_SetPasswordResponse_QNAME, SetPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUser }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.arubtsova.ru/", name = "updateUser")
    public JAXBElement<UpdateUser> createUpdateUser(UpdateUser value) {
        return new JAXBElement<UpdateUser>(_UpdateUser_QNAME, UpdateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.arubtsova.ru/", name = "updateUserResponse")
    public JAXBElement<UpdateUserResponse> createUpdateUserResponse(UpdateUserResponse value) {
        return new JAXBElement<UpdateUserResponse>(_UpdateUserResponse_QNAME, UpdateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewProfile }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.arubtsova.ru/", name = "viewProfile")
    public JAXBElement<ViewProfile> createViewProfile(ViewProfile value) {
        return new JAXBElement<ViewProfile>(_ViewProfile_QNAME, ViewProfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewProfileResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.arubtsova.ru/", name = "viewProfileResponse")
    public JAXBElement<ViewProfileResponse> createViewProfileResponse(ViewProfileResponse value) {
        return new JAXBElement<ViewProfileResponse>(_ViewProfileResponse_QNAME, ViewProfileResponse.class, null, value);
    }

}
