package ru.arubtsova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.service.ServiceLocator;
import ru.arubtsova.tm.enumerated.Role;
import ru.arubtsova.tm.model.Session;
import ru.arubtsova.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminEndpoint extends AbstractEndpoint {

    @NotNull
    private final ServiceLocator serviceLocator;

    public AdminEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public @NotNull User findById(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findById(id);
    }

    @WebMethod
    public User removeByLogin(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeByLogin(login);
    }

    @WebMethod
    public @NotNull User lockUserByLogin(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().lockUserByLogin(login);
    }

    @WebMethod
    public @NotNull User unlockUserByLogin(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @WebMethod
    public void loadBackup(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadBackup();
    }

    @WebMethod
    public void saveBackup(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveBackup();
    }

    @WebMethod
    public void loadBase64(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadBase64();
    }

    @WebMethod
    public void saveBase64(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveBase64();
    }

    @WebMethod
    public void loadBin(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadBin();
    }

    @WebMethod
    public void saveBin(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveBin();
    }

    @WebMethod
    public void loadJsonFasterXML(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadJsonFasterXML();
    }

    @WebMethod
    public void loadJsonJaxb(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadJsonJaxb();
    }

    @WebMethod
    public void saveJsonFasterXML(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveJsonFasterXML();
    }

    @WebMethod
    public void saveJsonJaxb(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveJsonJaxb();
    }

    @WebMethod
    public void loadXMLFasterXML(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadXMLFasterXML();
    }

    @WebMethod
    public void loadXMLJaxb(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadXMLJaxb();
    }

    @WebMethod
    public void saveXMLFasterXML(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveXMLFasterXML();
    }

    @WebMethod
    public void saveXMLJaxb(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveXMLJaxb();
    }

    @WebMethod
    public void loadYAMLFasterXML(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadYAMLFasterXML();
    }

    @WebMethod
    public void saveYAMLFasterXML(@Nullable @WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveYAMLFasterXML();
    }

}
