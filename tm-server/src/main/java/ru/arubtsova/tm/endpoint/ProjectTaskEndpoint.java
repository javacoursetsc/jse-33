package ru.arubtsova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.service.ServiceLocator;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.model.Session;
import ru.arubtsova.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectTaskEndpoint extends AbstractEndpoint {

    @NotNull
    private final ServiceLocator serviceLocator;

    public ProjectTaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public @NotNull List<Task> findAllTaskByProjectId(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().findAllTaskByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    public @Nullable Task bindTaskToProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "taskId") final String taskId,
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().bindTaskToProject(session.getUserId(), taskId, projectId);
    }

    @WebMethod
    public @Nullable Task unbindTaskFromProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "taskId") final String taskId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().unbindTaskFromProject(session.getUserId(), taskId);
    }

    @WebMethod
    public @Nullable Project removeProjectWithTasksById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeProjectWithTasksById(session.getUserId(), projectId);
    }

}
