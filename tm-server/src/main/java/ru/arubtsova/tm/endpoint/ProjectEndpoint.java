package ru.arubtsova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.service.ServiceLocator;
import ru.arubtsova.tm.enumerated.Status;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint {

    @NotNull
    private final ServiceLocator serviceLocator;

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public @NotNull List<Project> findAllProjectWithUserId(
            @Nullable @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @WebMethod
    public @NotNull Project findByIdProjectWithUserId(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findById(session.getUserId(), id);
    }

    @WebMethod
    public @NotNull Project findByIndexProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByIndex(session.getUserId(), index);
    }

    @WebMethod
    public @NotNull Project findByNameProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByName(session.getUserId(), name);
    }

    @WebMethod
    public void clearProjectWithUserId(
            @Nullable @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @WebMethod
    public @NotNull Project updateByIndexProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public @NotNull Project updateByIdProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    public @NotNull Project startByIndexProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startByIndex(session.getUserId(), index);
    }

    @WebMethod
    public @NotNull Project startByIdProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startById(session.getUserId(), id);
    }

    @WebMethod
    public @NotNull Project startByNameProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startByName(session.getUserId(), name);
    }

    @WebMethod
    public @NotNull Project finishByIndexProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    public @NotNull Project finishByIdProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().finishById(session.getUserId(), id);
    }

    @WebMethod
    public @NotNull Project finishByNameProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().finishByName(session.getUserId(), name);
    }

    @WebMethod
    public @NotNull Project changeStatusByIndexProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index,
            @NotNull @WebParam(name = "status") final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @WebMethod
    public @NotNull Project changeStatusByIdProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id,
            @NotNull @WebParam(name = "status") final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().changeStatusById(session.getUserId(), id, status);
    }

    @WebMethod
    public @NotNull Project changeStatusByNameProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name,
            @NotNull @WebParam(name = "status") final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().changeStatusByName(session.getUserId(), name, status);
    }

    @WebMethod
    public @Nullable Project removeByIdProjectWithUserId(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeById(session.getUserId(), id);
    }

    @WebMethod
    public @Nullable Project removeByIndexProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public @Nullable Project removeByNameProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeByName(session.getUserId(), name);
    }

    @WebMethod
    public @NotNull Project addProjectWithDescription(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().add(session.getUserId(), name, description);
    }

}
