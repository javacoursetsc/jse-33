package ru.arubtsova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.api.service.ServiceLocator;
import ru.arubtsova.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class SessionEndpoint extends AbstractEndpoint {

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public Session openSession(
            @NotNull @WebParam(name = "login", partName = "login") final String login,
            @NotNull @WebParam(name = "password", partName = "password") final String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }

    @WebMethod
    public void closeSession(
            @NotNull @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().close(session);
    }

}
