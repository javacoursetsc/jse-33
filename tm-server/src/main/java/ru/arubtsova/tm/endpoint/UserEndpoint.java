package ru.arubtsova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.service.ServiceLocator;
import ru.arubtsova.tm.model.Session;
import ru.arubtsova.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class UserEndpoint extends AbstractEndpoint {

    @NotNull
    private final ServiceLocator serviceLocator;

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public @NotNull User setPassword(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "password") final String password
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().setPassword(session.getUserId(), password);
    }

    @WebMethod
    public @NotNull User updateUser(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "firstName") final String firstName,
            @NotNull @WebParam(name = "lastName") final String lastName,
            @NotNull @WebParam(name = "middleName") final String middleName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUser(session.getUserId(), firstName, lastName, middleName);
    }

    @WebMethod
    public void register(
            @NotNull @WebParam(name = "login") final String login,
            @NotNull @WebParam(name = "password") final String password,
            @NotNull @WebParam(name = "email") final String email
    ) {
        serviceLocator.getAuthService().register(login, password, email);
    }

    @WebMethod
    public @NotNull User viewProfile(
            @Nullable @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findById(session.getUserId());
    }

}
