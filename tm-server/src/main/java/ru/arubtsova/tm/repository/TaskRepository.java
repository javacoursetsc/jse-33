package ru.arubtsova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.repository.ITaskRepository;
import ru.arubtsova.tm.model.Task;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @NotNull
    public static Predicate<Task> predicateByProjectId(@NotNull final String projectId) {
        return s -> projectId.equals(s.getProjectId());
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return map.values().stream()
                .filter(predicateByUserId(userId))
                .filter(predicateByProjectId(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        findAllByProjectId(userId, projectId).forEach(task -> map.remove(task.getId()));
    }

    @Nullable
    @Override
    public Task bindTaskToProject(
            @NotNull final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) {
        Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Nullable
    @Override
    public Task unbindTaskFromProject(@NotNull final String userId, final @NotNull String taskId) {
        @NotNull Task task = findById(userId, taskId);
        task.setProjectId(null);
        return task;
    }

}
