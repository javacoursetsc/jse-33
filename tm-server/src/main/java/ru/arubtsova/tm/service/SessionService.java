package ru.arubtsova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.repository.ISessionRepository;
import ru.arubtsova.tm.api.service.ISessionService;
import ru.arubtsova.tm.api.service.ServiceLocator;
import ru.arubtsova.tm.enumerated.Role;
import ru.arubtsova.tm.exception.authorization.AccessDeniedException;
import ru.arubtsova.tm.exception.empty.EmptyLoginException;
import ru.arubtsova.tm.exception.empty.EmptyPasswordException;
import ru.arubtsova.tm.model.Session;
import ru.arubtsova.tm.model.User;
import ru.arubtsova.tm.util.HashUtil;

import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionService(
            @NotNull ISessionRepository sessionRepository,
            @NotNull ServiceLocator serviceLocator
    ) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void validate(@Nullable final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        final String userId = session.getUserId();
        final @NotNull User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!contain(session.getId())) throw new AccessDeniedException();
    }

    @Nullable
    @Override
    public Session open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @NotNull final User user = serviceLocator.getUserService().findByLogin(login);

        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = serviceLocator.getUserService().findByLogin(login);
        if (user.isLocked()) throw new AccessDeniedException();
        @NotNull final String hash = user.getPasswordHash();
        @NotNull final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new AccessDeniedException();
        return passwordHash.equals(hash);
    }

    @Override
    public void signOutByLogin(@Nullable final String login, @NotNull final Session session) {
        if (login == null || login.isEmpty()) return;
        serviceLocator.getUserService().findByLogin(login);
        close(session);
    }

    @Override
    public void signOutByUserId(@Nullable final String userId, @NotNull final Session session) {
        if (userId == null || userId.isEmpty()) return;
        close(session);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @Nullable final String signature = HashUtil.sign(serviceLocator.getPropertyService(), session);
        session.setSignature(signature);
        return session;
    }

    @Nullable
    @Override
    public List<Session> getListSession(@NotNull final Session session) {
        return null;
    }

    @Override
    public void close(@NotNull final Session session) {
        validate(session);
        remove(session);
    }

}
