package ru.arubtsova.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.enumerated.Status;
import ru.arubtsova.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IService<E> {

    @NotNull
    List<E> findAll(@Nullable String userId);

    @Nullable
    List<E> findAll(@NotNull String userId, @Nullable Comparator<E> comparator);

    @Nullable
    E add(@Nullable String userId, @Nullable E entity);

    @NotNull
    E findById(@Nullable String userId, @Nullable String id);

    @NotNull
    E findByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    E findByName(@Nullable String userId, @Nullable String name);

    void clear(@Nullable String userId);

    @NotNull
    E updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    E updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    E startByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    E startById(@Nullable String userId, @Nullable String id);

    @NotNull
    E startByName(@Nullable String userId, @Nullable String name);

    @NotNull
    E finishByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    E finishById(@Nullable String userId, @Nullable String id);

    @NotNull
    E finishByName(@Nullable String userId, @Nullable String name);

    @NotNull
    E changeStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @NotNull Status status
    );

    @NotNull
    E changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @NotNull Status status
    );

    @NotNull
    E changeStatusByName(
            @Nullable String userId,
            @Nullable String name,
            @NotNull Status status
    );

    void remove(@Nullable String userId, @Nullable E entity);

    @Nullable E removeById(@Nullable String userId, @Nullable String id);

    @Nullable E removeByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable E removeByName(@Nullable String userId, @Nullable String name);

}
