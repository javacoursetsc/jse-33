package ru.arubtsova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.IRepository;
import ru.arubtsova.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    void exclude(@NotNull Session session);

    @Nullable
    Session findByUserId(String userId);

    @NotNull
    List<Session> findAllSession(@NotNull String userId);

}
