package ru.arubtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.enumerated.Role;
import ru.arubtsova.tm.model.User;

public interface IAuthService {

    @NotNull
    User getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void checkRole(@Nullable Role... roles);

    void logout();

    void register(@NotNull String login, @NotNull String password, @NotNull String email);

}
