package ru.arubtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.IService;
import ru.arubtsova.tm.enumerated.Role;
import ru.arubtsova.tm.model.Session;

import java.util.List;

public interface ISessionService extends IService<Session> {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    @Nullable
    Session open(@Nullable String login, @Nullable String password);

    @Nullable
    Session sign(@NotNull Session session);

    void validate(@NotNull Session session, Role role);

    void signOutByLogin(@Nullable String login, @NotNull Session session);

    void signOutByUserId(@Nullable String userId, @NotNull Session session);

    @Nullable
    List<Session> getListSession(@NotNull Session session);

    void close(@NotNull Session session);

    void validate(@NotNull Session session);

}
