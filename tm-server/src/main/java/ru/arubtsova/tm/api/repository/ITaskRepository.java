package ru.arubtsova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.IBusinessRepository;
import ru.arubtsova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    Task bindTaskToProject(@NotNull String userId, @Nullable String taskId, @Nullable String projectId);

    @Nullable
    Task unbindTaskFromProject(@NotNull String userId, @NotNull String projectId);

}
